<?php
	function createDictionary($str) {
		$dictionary = array();
		$length = strlen($str);

		for ($i = 0; $i < $length; $i++) {
			if (!isInDictionary($dictionary, $str[$i])) {
				$dictionary[] = $str[$i];
			}
		}

		return $dictionary;
	}

	function isInDictionary($dictionary, $char) {
		foreach ($dictionary as $ch) {
			if ($ch == $char) {
				return true;
			}
		}

		return false;
	}

	function getItemIndex($dictionary, $char) {
		$length = count($dictionary);

		for ($i = 0; $i < $length; $i++) {
			if ($dictionary[$i] == $char) {
				return $i;
			}
		}
	}

	function stringToNumbersArray($dictionary, $str) {
		$numbers = array();
		$length = strlen($str);

		for ($i = 0; $i < $length; $i++) {
			$numbers[] = getItemIndex($dictionary, $str[$i]);
		}

		return $numbers;
	}

	function getWordSize($length) {
		return ceil(log($length, 2));
	}

	function bitsToNumber($arr) {
		$n = intval(implode('', $arr), 2);

		return $n;
	}

	function decreaseKey($key, $neededLength) {
		$arrKey = array();
		$length = strlen($key);

		for ($i = 0; $i < $length; $i++) {
			$arrKey[] = $key[$i];
		}

		$iter = count($arrKey) - $neededLength;

		for ($i = 0; $i < $iter; $i++) {
			$encChar = array_pop($arrKey);
			$length = count($arrKey);

			for ($j = 0; $j < $length; $j++) {
				$currentChar = $arrKey[$j];
				$arrKey[$j] = xorData($currentChar, $encChar);
				$encChar = $currentChar;
			}
		}

		$data = '';

		return implode('', $arrKey);
	}

	function increaseKey($key, $minLength) {
		while (strlen($key) < $minLength) {
			$key .= $key;
		}
		
		return $key;
	}

	function alignKey($key, $length) {
		if (!$key) {
			return $key;
		}

		if (strlen($key) < $length) {
			$key = increaseKey($key, $length);
		}

		if (strlen($key) > $length) {
			$key = decreaseKey($key, $length);
		}

		return $key;
	}

	function getServiceDataLength($str) {
		$dictionarySize = explode(';', $str)[0];

		return substr($str, 0, strpos($str, ':') + $dictionarySize + 1);
	}

	function getServiceData($str) {
		$data = getServiceDataLength($str);

		return $data;
	}

	function xorData($ch1, $ch2) {
		$buff;

		$buff = ord($ch1) ^ ord($ch2);
		$buff = $buff & 0x7FFFFFFF;
		$buff = chr($buff);

		return $buff;
	}

	function encode($str, $key) {
		$serviceData = getServiceData($str);
		$encodedServiceData = '';
		$key = alignKey($key, strlen($serviceData));

		if (!$key) {
			return $str;
		}

		$length = strlen($key);


		for ($i = 0; $i < $length; $i++) {
			$encodedServiceData .= xorData($serviceData[$i], $key[$i]);
		}

		$str = strlen($serviceData) . ';' . $encodedServiceData . substr($str, strlen($serviceData));

		return $str;
	}

	function decode($str, $key) {
		$serviceDataSize = explode(';', $str)[0] * 1;
		$data = substr($str, strpos($str, ';') + 1);
		$serviceData = substr($data, 0, $serviceDataSize);
		$decodedServiceData = '';
		$key = alignKey($key, $serviceDataSize);

		if (!$key) {
			return $str;
		}

		for ($i = 0; $i < $serviceDataSize; $i++) {
			$decodedServiceData .= xorData($serviceData[$i], $key[$i]);
		}

		$data = $decodedServiceData . substr($data, $serviceDataSize);

		return $data;
	}

	function compress($str) {
		$dictionary = createDictionary($str);
		$numbers = stringToNumbersArray($dictionary, $str);
		$wordSize = getWordSize(count($dictionary));
		$bitBuffer = array();
		$dest = '';

		foreach ($numbers as $number) {
			for ($i = 0; $i < $wordSize; $i++) {
				$bit = $number & pow(2, $i);
				$bit = !!$bit * 1;
				$bitBuffer[] = $bit;

				if (count($bitBuffer) == 8) {
					$dest .= chr(bitsToNumber($bitBuffer));
					$bitBuffer = array();
				}
			}
		}

		if (count($bitBuffer)) {
			$needElements = 8 - count($bitBuffer);

			for ($i = 0; $i < $needElements; $i++) {
				$bitBuffer[] = 0;
			}
			$dest .= chr(bitsToNumber($bitBuffer));
		}

		if (strlen($dest)) {
			$dest = count($dictionary) . ';' . strlen($str) . ';:' . implode('', $dictionary) . $dest;
		}

		return $dest;
	}

	function uncompress($source) {
		$str = explode(';', $source, 3);
		$dictionarySize = $str[0] * 1;
		$originalSize = $str[1] * 1;
		$dictionary = substr($source, strpos($source, ':') + 1, $dictionarySize);
		$wordSize = (int) getWordSize($dictionarySize);
		$bitBuffer = array();
		$str = substr($source, strpos($source, ':') + 1 + $dictionarySize);
		$bitCounts = 0;
		$res = '';
		$length = strlen($str);

		for ($i = 0; $i < $length; $i++) {
			for ($j = 7; $j >= 0; $j--) {
				$charCode = ord($str[$i]);
				$bit = pow(2 ,$j) & $charCode;
				$bit = !!$bit * 1;
				$bitBuffer[] = $bit;
				$bitCounts++;
				if ($bitCounts == $wordSize) {
					$wordNumber = bitsToNumber(array_reverse($bitBuffer));
					$res .= $dictionary[$wordNumber];
					$bitBuffer = array();
					$bitCounts = 0;
					$originalSize--;
				}

				if ($originalSize == 0) {
					break;
				}
			}
		}

		return $res;
	}

	$encodeKey;
	$decodeKey;

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$testData = "'red/apple/candy/jujube/red','green/apple/candy','red/garnet/juice','green/grapes/candy','orange/orange/coctail/alcohol','green/apple/jujube/green','green/grapes/ice cream','red/apple/candy','orange/orange/juice','orange/orange/pie','red/strawberry/candy','red/strawberry/pie','orange/orange/coctail/non alcohol','red/apple/puree','green/grapes/juice','green/grass/silo','red/strawberry/juice','red/apple/candy/jujube/green','red/garnet/candy/red','red/grapes/ice cream','red/garnet/candy/jujube','red/apple/candy/jujube/blue','green/apple/jujube/green','red/strawberry/ice cream','red/apple/ice cream','red/garnet/pie','green/grass/hay','green/lime/coctail/non alcohol','red/grapes/juice','green/lime/coctail/alcohol','green/lime/tea','green/apple/puree','green/lime/mousse','red/apple/candy/sweet','green/apple/sweet','red/grapes/candy'";
		$encodeKey = $_POST['encode-key'];
		$decodeKey = $_POST['decode-key'];
		$uncom = $_POST['uncompressed-string'];
		if (!strlen($uncom)) {
			$uncom = $testData;
		}
		$com = compress($uncom);
		$encoded = encode($com, $encodeKey);
		$decoded = decode($encoded, $decodeKey);
		$decom = uncompress($decoded);

		if ($com && $decom) {
			$compression = (strlen($com) / strlen($decom)) * 100;
		}
	}

?><!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Compressor</title>
	<style>
		form {
			max-width: 800px;
			margin: auto;
		}

		h1 {
			font-size: 16px;
		}

		textarea {
			resize: vertical;
			width: 100%;
			height: 100px;
		}
	</style>
</head>
<body>
	<form method="post">
		<div>
			<h1>Незапакованная строка длиной: <?php echo strlen($uncom) ?> символов</h1>
			<textarea name="uncompressed-string" id="uncompressed-string"><?php echo $uncom; ?></textarea>
		</div>
		<div>
			<h1>Запакованная строка длиной: <?php echo strlen($com); ?> символов</h1>
			<textarea id="compressed-string"><?php echo $com; ?></textarea>
		</div>
		<div>
			<h1>Запакованная и зашифрованная строка длиной: <?php echo strlen($encoded); ?> символов</h1>
			<p>
				<input type="text" name="encode-key" value="<?php echo $encodeKey; ?>">
				ключ для шифрования
			</p>
			<textarea id="compressed-string"><?php echo $encoded; ?></textarea>
		</div>
		<div>
			<h1>Запакованная и расшифрованная строка длиной: <?php echo strlen($decoded); ?> символов</h1>
			<p>
				<input type="text" name="decode-key" value="<?php echo $decodeKey; ?>"> ключ для расшифрования
			</p>
			<textarea id="compressed-string"><?php echo $decoded; ?></textarea>
		</div>
		<div>
			<h1>Распавоканная строка длиной: <?php echo strlen($decom); ?> символов</h1>
			<textarea id="decompressed-string"><?php echo $decom; ?></textarea>
		</div>
		<div>
			Сжатие: <?php echo (int) $compression; ?>%
		</div>
		<button type="submit">Сжать</button>
	</form>
</body>
</html>